 <?php

  /**
  * @file
  * Configuration settings page to check if the library is detected.
  */

  /**
  * Config form.
  */
  function plotly_js_admin_settings() {
  $form = array();
  $library = plotly_js_get_if_installed();

  $form['plotly_js']['plotly_js_library_available'] = array(
    '#type' => 'markup',
    '#markup' => $library['installed'] ?
    t('Library is available. (Version: @version)', array('@version' => $library['version'])) :
    t('Library is not available. To install it use "drush plotlydl" or download the project from the <a href="@url">git repository</a>.', array('@url' => 'https://github.com/plotly/plotly.js/archive/master.zip')),
    '#suffix' => '<hr>',
  );
  $form = system_settings_form($form);
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Check library'));

  return $form;
}
