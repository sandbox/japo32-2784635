/**
* @file
* Javascript to integrate the plotly.js library with Drupal.
*/

(function ($) {
  'use strict';

  // Make plotly accessible to the drupal object.
  Drupal.plotly = Plotly;

  Drupal.behaviors.plotlyjs = {
    attach: function (context, settings) {
    }
  };
}(jQuery));
