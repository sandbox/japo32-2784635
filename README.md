Summary
=======

Integrate the open source Plotly.js library with Drupal.

## Plotly.js

Built on top of d3.js and stack.gl, plotly.js is a high-level, declarative charting library. plotly.js ships with 20 chart types, including 3D charts, statistical graphs, and SVG maps.

[Plotly JS](https://plot.ly/javascript/)
[Github Repository](https://github.com/plotly/plotly.js)

## Requirements

* [Libraries API 2](http://drupal.org/project/libraries)

## Installation for D7

The library can be downloaded into the libraries directory using the drush command `drush plotlydl`.

## Usage

As of the moment, this module only serves to include the library.

To load the 'Plotly' object, add this to your code:

    plotly_js_load()

Once enabled, a dev can use the `Plotly` object.

[Start plotting.](https://plot.ly/javascript/getting-started/#start-plotting)
